from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from .models import Book


class BooksListView(LoginRequiredMixin, ListView):
    template_name = 'books/books_list.html'
    context_object_name = 'books'
    model = Book
    login_url = 'account_login'


class BooksDetailView(PermissionRequiredMixin, LoginRequiredMixin, DetailView):
    model = Book
    context_object_name = 'book'
    template_name = 'books/book_detail.html'
    login_url = 'account_login'
    permission_required = 'books.special_status'

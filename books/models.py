from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model
import uuid


class Book(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
    )
    author = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    price = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to='images/', blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('book_detail', args=[str(self.id)])

    class Meta:
        permissions = [
            ('special_status', 'Can read books'),
        ]


class Review(models.Model):
    review = models.TextField()
    book = models.ForeignKey(
        Book,
        on_delete=models.PROTECT,
        related_name='reviews',
    )
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.review

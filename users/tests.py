from django.test import TestCase
from django.contrib.auth import get_user_model


class CustomUserTests(TestCase):
    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(
            username='akim',
            email='da@',
            password='testpass123',
        )
        self.assertEqual(user.username, 'akim')
        self.assertEqual(user.email, 'da@')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)

    def test_create_superuser(self):
        User = get_user_model()
        user = User.objects.create_superuser(
            username='akimsuper',
            email='superda@',
            password='testpass123',
        )
        self.assertEqual(user.username, 'akimsuper')
        self.assertEqual(user.email, 'da@')
        self.assertTrue(user.is_active)
        self.assertTrue(user.is_staff)

